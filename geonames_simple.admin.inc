<?php

function geonames_simple_settings($form, &$form_state)
{

  $form['geonames_simple_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Geonames username'),
    '#default_value' => variable_get('geonames_simple_username'),
    '#required' => TRUE,
    '#description' => t('This is *required* for commercial and free services. You can !register.', array('!register' => l('register a GeoNames account', 'http://www.geonames.org/login'))),
  );
  $form['geonames_simple_lang'] = array(
    '#type' => 'textfield',
    '#title' => t('Wikipedia language to return results'),
    '#default_value' => variable_get('geonames_simple_lang', 'en'),
    '#required' => TRUE,
    '#description' => t('The language to fetch article links from'),
  );

  return system_settings_form($form);
}
