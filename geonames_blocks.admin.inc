<?php

function geonames_blocks_settings($form, &$form_state)
{

  $form['geonames_blocks_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of blocks'),
    '#default_value' => variable_get('geonames_blocks_number', '1'),
    '#required' => TRUE,
    '#description' => t('Enter number of blocks you want control over'),
  );
  $i = 0;
  $num = variable_get('geonames_blocks_number');
  if (empty($num)) { $num = 1; }
  while ($i < $num) {
    $use = $i + 1;
    $form['geonames_blocks_block_set' . $i] = array(
      '#type' => 'vertical_tabs',
      '#title' => t('Block number') . ' ' . $use,
      '#prefix' => t('Block number') . ' ' . $use,
    );
    $form['geonames_blocks_block_set' . $i]['first'] = array(
      '#type' => 'fieldset',
      '#title' => t('General settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['geonames_blocks_block_set' . $i]['first']['geonames_blocks_type_' . $i] = array(
      '#type' => 'select',
      '#title' => 'Field to use for input',
      '#default_value' => variable_get('geonames_blocks_type_' . $i, 'geonames_simple_wiki_location'),
      '#description' => t('Type of query to send to geonames'),
      '#options' => array('geonames_simple_wiki_location' => 'Related Wikipedia articles based on lat/lon', 'geonames_simple_wiki_postalcode' => 'Related Wikipedia articles based on postal code'), 
    );
    $form['geonames_blocks_block_set' . $i]['first']['geonames_blocks_ol_field_' . $i] = array(
      '#type' => 'textfield',
      '#title' => 'Openlayers field to use for input',
      '#default_value' => variable_get('geonames_blocks_ol_field_' . $i),
      '#description' => t('Full machine readable name of an openlayers field to use (only usable for lat/lon lookup). Remember to enable the block only on node types that has this field.'),
    );
    $form['geonames_blocks_block_set' . $i]['first']['geonames_blocks_other_field_' . $i] = array(
      '#type' => 'textfield',
      '#title' => 'Other field to use for input',
      '#default_value' => variable_get('geonames_blocks_other_field_' . $i),
      '#description' => t('Full machine readable name of a field to use. Remember to enable the block only on node types that has this field.'),
    );
    $form['geonames_blocks_block_set' . $i]['first']['geonames_blocks_php_' . $i] = array(
      '#type' => 'textarea',
      '#title' => 'Custom PHP code to use for input',
      '#default_value' => variable_get('geonames_blocks_php_' . $i),
      '#description' => t('PHP code that returns lat/lon. <strong>DO NOT USE THIS IF YOU DON\' KNOW WHAT YOU ARE DOING!</STRONG> This will be used over field value if you have written something here. Useful if your block does not have access to plain values. <br />Should return an array with [\'lat\'] and [\'lon\'] on lat/lon lookup. On every other service return plain value.'),
    );
    $form['geonames_blocks_block_set' . $i]['second'] = array(
      '#type' => 'fieldset',
      '#title' => t('Output of related content'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['geonames_blocks_block_set' . $i]['second']['geonames_blocks_output_' . $i] = array(
      '#type' => 'radios',
      '#title' => 'Output of block',
      '#default_value' => variable_get('geonames_blocks_output_' . $i, 'links'),
      '#options' => array('map' => t('Show on a map'), 'links' => t('Show as links')),
      '#description' => t('Map output requires') . l(' Gmap module.', 'http://drupal.org/project/gmap', array('attributes' => array('target' => '_blank'))),
    );
    $i++;
  } 

  return system_settings_form($form);
}
